export default function PopupSelected({
  display,
  count,
  onMarkAsArrived,
  onDelete,
}) {
  return (
    <div className={`popup ${display ? "show" : "hide"}`}>
      <span>x</span>
      <p>{count} Row Selected</p>
      <button className="btn" onClick={onMarkAsArrived}>
        Mark as arrived
      </button>
      <button className="btn btn-delete" onClick={onDelete}>
        {" "}
        Delete Table
      </button>
    </div>
  );
}
