import { Bar } from "react-chartjs-2";

export default function ({ data }) {
  const count = [
    data.filter((obj) => obj.category === "Category 1").length,
    data.filter((obj) => obj.category === "Category 2").length,
    data.filter((obj) => obj.category === "Category 3").length,
    data.filter((obj) => obj.category === "Category 4").length,
  ];

  const barData = {
    labels: ["Category 1", "Category 2", "Category 3", "Category 4"],
    datasets: [
      {
        label: "Category",
        data: count,
        backgroundColor: [" #ff6b21"],
      },
    ],
  };

  const options = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  };

  return (
    <div className="bar-chart card">
      <h5 className="title">Category Ratio</h5>
      <div className="row legend">
        <div className="col-2 content">
          <p className="label">Category 1</p>
          <p className="count">{count[0]}</p>
        </div>
        <div className="col-2 content">
          <p className="label">Category 2</p>
          <p className="count">{count[1]}</p>
        </div>
        <div className="col-2 content">
          <p className="label">Category 3</p>
          <p className="count">{count[2]}</p>
        </div>
        <div className="col-2 content">
          <p className="label">Category 4</p>
          <p className="count">{count[3]}</p>
        </div>
      </div>
      <Bar data={barData} width={100} height={50} options={options} />
    </div>
  );
}
