import React, { useState } from "react";

export default function Form({ onSubmit }) {
  const [data, setData] = useState({
      name: "",
      arrive: "Hasn't Arrival",
      category: "Category 1",
      availability: ""
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit(data);
    setData({
      name: "",
      arrive: "Hasn't Arrival",
      category: "Category 1",
      availability: ""
    })
  };

  return (
    <form onSubmit={handleSubmit} className="card bg-gray">
      <div className="form mt-0">
        <label>Name</label>
        <input
          type="text"
          placeholder="Enter Your Name"
          value={data.name}
          onChange={(e) => setData({ ...data, name: e.target.value })}
        />
      </div>
      <div className="form">
        <label placeholder="Select Category">Category</label>
        <select
          name="category"
          id="category"
          onChange={(e) => setData({ ...data, category: e.target.value })}
        >
          <option value="Category 1">Category 1</option>
          <option value="Category 2">Category 2</option>
          <option value="Category 3">Category 3</option>
          <option value="Category 4">Category 4</option>
        </select>
      </div>
      <div className="form-radio">
        <label>Availability</label>
        <div className="row">
          <label className="col-5 radio">
            <input
              type="radio"
              name="availability"
              value="Available"
              checked={data.availability==="Available"}
              onChange={(e) =>
                setData({ ...data, availability: e.target.value })
              }
            />
            Available
          </label>
          <label className="col-5 radio">
            <input
              type="radio"
              name="availability"
              value="Full"
              checked={data.availability==="Full"}
              onChange={(e) =>
                setData({ ...data, availability: e.target.value })
              }
            />
            Full
          </label>
        </div>
      </div>
      <div className="form">
        <label>Arrive</label>
        <input
          type="text"
          name="arrive"
          placeholder="Value Hasn't Arrival"
          value={data.arrive}
          readOnly={true}
        />
      </div>
      <button className="btn mt-2" type="submit">
        Submit Form
      </button>
    </form>
  );
}
