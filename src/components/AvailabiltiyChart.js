import { Doughnut, Pie } from "react-chartjs-2";

export default function PieChart({ data }) {
  const count = [
    data.filter((obj) => obj.availability === "Available").length,
    data.filter((obj) => obj.availability === "Full").length,
  ];
  const dataChart = {
    labels: ["Available", "Full"],
    datasets: [
      {
        label: "# of Votes",
        data: count,
        backgroundColor: ["#025e3e", "#ff6b21"],
      },
    ],
  };
  return (
    <div className="pie-chart card">
      <h5 className="title">Availability Ratio</h5>
      <Doughnut data={dataChart} />
    </div>
  );
}
