import React, { useEffect, useState } from "react";

export default function Table({ data, onChecked, checked, setChecked }) {
  const handleCheck = (row) => {
    const isChecked = isRowChecked(row);
    if (isChecked > -1) {
      let temp = checked.filter((check) => check?.id != row?.id);
      setChecked(temp);
    } else {
      setChecked([...checked, row]);
    }
  };

  useEffect(() => {
    onChecked(checked);
  }, [checked]);

  const isRowChecked = (row) => {
    return checked.findIndex((check) => check.id == row?.id);
  };
  return (
    <table className="separate">
      <thead>
        <tr>
          <td style={{ width: 5 + "%" }}>
            <input
              type="checkbox"
              onChange={() =>
                checked.length > 0 && checked.length == data.length
                  ? setChecked([])
                  : setChecked(data)
              }
              checked={checked.length == data.length}
            />
          </td>
          <td>ID</td>
          <td>Name</td>
          <td>Category</td>
          <td>Availability</td>
          <td>Arrival</td>
        </tr>
      </thead>
      <tbody>
        {data.map((row, key) => (
          <tr key={key} className={isRowChecked(row) > -1 ? "checked" : ""}>
            <td style={{ width: 5 + "%" }}>
              <input
                type="checkbox"
                onChange={() => handleCheck(row)}
                checked={isRowChecked(row) > -1}
              />
            </td>
            <td>{row.id}</td>
            <td>{row.name}</td>
            <td>{row.category}</td>
            <td>{row.availability}</td>
            <td>{row.arrive}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}
