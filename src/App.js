import React, { useEffect, useState } from "react";
import Form from "./components/Form";
import Table from "./components/Table";
import { Bar } from "react-chartjs-2";

import "./App.scss";
import PopupSelected from "./components/PopupSelected";
import BarChart from "./components/CategoryChart";
import PieChart from "./components/AvailabiltiyChart";
import CategoryChart from "./components/CategoryChart";

function App() {
  const [data, setData] = useState([]);
  const [checked, setChecked] = useState([]);

  const handleMarkAsArrived = () => {
    setData(
      data.map((row) =>
        checked.findIndex((rowCheck) => rowCheck.id == row.id) > -1
          ? { ...row, arrive: "Arrived" }
          : row
      )
    );
    setChecked([]);
  };

  const handleDelete = () => {
    console.log("checked", checked);
    const newData = data.filter((row) => {
      return checked.findIndex((rowCheck) => rowCheck.id == row.id) < 0;
    });
    setData(newData);
    setChecked([]);
  };

  const barData = (canvas) => {
    const ctx = canvas.getContext("2d");
    const gradient = ctx.createLinearGradient(0, 0, 100, 0);

    return {
      backgroundColor: gradient,
      data,
    };
  };

  return (
    <div className="container">
      <h3 className="title">Charts and Table Visualization</h3>
      <div className="row gap-12">
        <div className="col-2.5">
          <Form
            onSubmit={(form) =>
              setData([
                ...data,
                {
                  id:
                    data.length > 0
                      ? Math.max.apply(
                          Math,
                          data.map(function (o) {
                            return o.id;
                          })
                        ) + 1
                      : 1,
                  ...form,
                },
              ])
            }
          />
        </div>
        <div className="col-5">
          <CategoryChart data={data} />
        </div>
        <div className="col-2.5">
          <PieChart data={data} />
        </div>
      </div>
      <div>
        <Table
          data={data}
          onChecked={(dataChecked) => setChecked(dataChecked)}
          checked={checked}
          setChecked={setChecked}
        />
        <PopupSelected
          count={checked.length}
          display={checked.length > 0}
          onMarkAsArrived={handleMarkAsArrived}
          onDelete={handleDelete}
        />
      </div>
    </div>
  );
}

export default App;
